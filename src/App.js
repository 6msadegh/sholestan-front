import Home from "./pages/Home/Home"
import {BrowserRouter as Router,Switch, Route} from "react-router-dom";
import Login from "./pages/Login/Login";
import Signup from "./pages/Signup/Signup";
import ForgetPass from "./pages/Login/ForgetPass";
import Profile from "./pages/Profile/Profile";
import Plan from "./pages/Plan/Plan";
import Courses from "./pages/Courses/Courses";
import Authorization from "./pages/General/Authorization";
import RecoverPass from "./pages/Login/RecoverPass";



function App() {
  return (
          <Router>
            <Switch>
                <Route exact path="/" component={()=>Authorization.getUserToken()?<Profile/>:<Home/>} />
                <Route exact path="/login" component={()=>Authorization.getUserToken()?<Profile/>:<Login/>} />
                <Route exact path="/signup" component={()=>Authorization.getUserToken()?<Profile/>:<Signup/>} />
                <Route exact path="/forgetpass" component={()=>Authorization.getUserToken()?<Profile/>:<ForgetPass/>} />
                <Route exact path="/changepass" render={props => <RecoverPass {...props}/> } />

                <Route exact path="/profile" component={()=>Authorization.getUserToken()?<Profile/>:<Home/>} />
                <Route exact path="/plan" component={()=>Authorization.getUserToken()?<Plan/>:<Home/>} />
                <Route exact path="/courses" component={()=>Authorization.getUserToken()?<Courses/>:<Home/>} />
            </Switch>
          </Router>
  );
}

export default App;
