import React, {useState} from "react";
import {Carousel} from "react-bootstrap";
import coverphoto from "../../Assets/Images/coverphoto.jpg";

function Slider() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };
    return (
        <Carousel activeIndex={index} onSelect={handleSelect}>
            <Carousel.Item>
                <img
                    className="d-block w-100 coverimage"
                    src={coverphoto}
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h3>بلبستان</h3>
                    <p>بهار۱۴۰۰</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100 coverimage"
                    src={coverphoto}
                    alt="Third slide"
                />

                <Carousel.Caption>
                    <h3>بلبلستان</h3>
                    <p>
                        بهار ۱۳۹۹
                    </p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
}

export default Slider