import React,{Component} from "react";

import "./Profile.css"
import {Col} from "react-bootstrap";
import Grade from "./Grade";
import {toFarsiNumber} from "../General/Translator";

class Report extends Component
{
    constructor(props) {
        super(props);


        this.state =
            {
                Term: "",
                Grades: [],
                course_id : 0,
            }
    }

    componentDidMount()
    {
        this.setState({ Term: this.props.report.term , Grades:this.props.report.grades});
    }

    render() {
        return (
            <div>
                <br/>
                <Col lg = "11" md="12" className="report mx-auto">
                    <div className="report_title_box">
                        <div className="repor_title_text mx-auto">کارنامه-ترم‌‌‌‌‌  {toFarsiNumber(this.state.Term)}</div>
                    </div>
                    {console.log(this.state.Grades)}

                    {this.state.Grades.map((course,i=0 ) => {i+=1; return <div key={course.code}>
                        <Grade course = {course}  course_id = {i} />
                    </div>})}
                    <br/>
                    <div className="col-md-12 mt-3">
                        <div className="row ">
                            <div className="col-lg-10 col-md-9"> </div>
                            <div className="col-lg-2 col-md-3 avg">معدل کل : {toFarsiNumber(this.props.report.termAvg.toFixed(2))} </div>
                        </div>
                    </div>
                </Col>
                <br/>

            </div>
        );
    }

}

export default Report