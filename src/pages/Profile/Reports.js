import React,{Component} from "react";
import axios from "axios";
import "./Profile.css"
import "./../Courses/Courses.css"
import Report from "./Report";


class Reports extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                Reports : []
            }

    }

       async componentDidMount() {
           await axios.get('http://87.247.185.122:31003/student/grades',)
                .then(response => {
                    console.log(response)
                    this.setState({ Reports: response.data});})
                    .catch(error => {console.log("log in error",error);});
    }

    render() {
        return (
            <div>
                {this.state.Reports.reverse().map((report) => { return <div key={report.term}>
                    <Report report={report}  />
                </div>})}
            </div>
        );
    }

}

export default Reports