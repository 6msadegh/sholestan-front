import React,{Component} from "react";

import "./Profile.css"
import {Col, Row} from "react-bootstrap";
import {toFarsiNumber} from "../General/Translator";

class Grade extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                id : "",
                grade : "",
                code : " ",
                name : "",
                classcode : "",
                situation : "",
                situation_style : "",
                grade_style : ""
            }
    }

    componentDidMount() {

        if(this.props.course.grade >= 10)
        {
            this.setState({situation:"قبول" , situation_style: "pass" , grade_style :"passgrade" })
        }else if(this.props.course.grade < 10)
        {
            this.setState({situation:"مردود" , situation_style: "fail" ,grade_style :"failgrade" })
        }else
        {
            this.setState({situation:"نامشخص" , situation_style: "unknown"})
        }
    }


    render() {
        return (
            <div>
                <Row className="dataContainer mx-auto">
                    <Col md = "12" className="mt-3">
                        <Row className="rowstyle ">
                            <Col lg="1" md="1" className="num-cell"> {toFarsiNumber(this.props.course_id)} </Col>
                            <Col lg="2" md="2"className="cell"> {toFarsiNumber(this.props.course.code)}-{toFarsiNumber(this.props.course.classCode)} </Col>
                            <Col lg="3" md="4"className="cell"> {this.props.course.courseName} </Col>
                            <Col lg="2" md="2"className="cell">{toFarsiNumber(this.props.course.units)} واحد </Col>
                            <Col lg="2" md="2"className="cell">
                                <Col lg="12" md="12" className={this.state.situation_style}> {this.state.situation} </Col>
                            </Col>
                            <Col lg="2" md="1" className={this.state.grade_style}> {toFarsiNumber(this.props.course.grade)} </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }

}
export default Grade