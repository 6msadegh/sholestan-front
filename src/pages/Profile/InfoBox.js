import React, {Component} from 'react';
import axios from "axios";
import './Profile.css';
import {toFarsiNumber} from "../General/Translator";

class InfoBox  extends Component
{
    constructor(props) {
        super(props);

    }

    componentDidMount()
    {
        console.log("salam2")
        console.log(this.props.Data)
        console.log("salam2")
    }

    render() {
        return(
            <>
                {/*<div className="profilebox">*/}
                {/*    <img className="profilephoto" src={this.props.Data.img} alt="Logo" />*/}
                {/*</div>*/}
                <br/><br/>
                <div className="info">
                    <p>نام:<span className="title"> {this.props.Data.firstName + " " + this.props.Data.lastName}</span></p>
                    <p>شماره دانشجویی: <span className="title">{toFarsiNumber(this.props.Data.id)}</span></p>
                    <p>تاریخ تولد: <span className="title"> {toFarsiNumber(this.props.Data.birthDate)}</span></p>
                    {/*<p>معدل کل:<span className="title"> {toFarsiNumber(this.props.Data.totalAvg)}</span></p>*/}
                    {/*<p>واحد گذرانده:<span className="title"> {toFarsiNumber(this.props.Data.totalPassedUnits)}</span></p>*/}
                    <p>دانشکده:<span className="title uni">{this.props.Data.faculty}  </span></p>
                    <p>رشته:<span className="title"> {this.props.Data.field} </span></p>
                    <p>مقطع:<span className="title"> {this.props.Data.level} </span></p>
                    <div className = "status"> <p>  مشغول به تحصیل </p>  </div>
                </div>
            </>
        );
    }
}

export default InfoBox;