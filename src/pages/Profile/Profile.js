import React, {Component} from 'react';
import {Button, Carousel, Col, Container, Row} from 'react-bootstrap';
import './Profile.css';
import Header from "../General/Header/Header";
import Spinner from "../General/Spinner/Spinner";
import InfoBox from "./InfoBox";
import Slider from "./Slider";
import Footer from "../General/Footer/Footer";
import Reports from "./Reports";


class Profile  extends Component
{
    constructor(props) {
        super(props);
    }



    componentDidMount()
    {
        document.title = "پروفایل";
    }

    render() {
        return(
            <div>

                <div className="emptybox"> </div>
                <Spinner />
                <Header Data = {this.props.Data}  FirstLink = "انتخاب واحد" SecondLink = "برنامه هفتگی" />
                <Slider />

                <Container fluid >
                    <Row>
                        <Col md = "3" lg = "3" className="rightside" >
                            <br/><br/>

                            <InfoBox Data = {JSON.parse(localStorage.getItem('data'))}/>
                        </Col>
                        <Col md = "9" lg = "9" >
                            {/*<Reports/>*/}
                        </Col>
                    </Row>

                </Container >
                <Footer/>
            </div>
        );
    }
}

export default Profile;