import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { Button } from 'react-bootstrap';
import logo from '../../Assets/Images/logo.png';
import '../Login/Login.css';
import './Signup.css';
import axios from "axios";
import {Link} from "react-router-dom";
import {validateID} from "../General/Validator";



class Signup extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state =
            {
                firstname : "",
                lastname : "",
                email : "",
                password : "" ,
                id : "",
                loginerror : ""
            };
    }

    componentDidMount() {
        document.title = "ثبت نام";
    }

    handleSignupSucces()
    {
        const message = <h6>ثبت نام موفقیت‌آمیز بود به صفحه وررد بروید.</h6>;
        const element1 = <h6> </h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(message, document.getElementById('message'));
    }

    handleSignupSuccesError()
    {
        console.log("salam");
        const element1 = <h6>دانشجویی با این مشخصات قبلا ثبت نام کرده :)</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
    }
    handleinvalidID()
    {
        console.log("salam");
        const element1 = <h6>شماره دانشجویی نامعتبر است</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
    }

    handleChange(event){
        this.setState({
            [event.target.name] : event.target.value
        })
        console.log("salam")
    }

    signupHandler(event)
    {
        const ID = this.state.id;

        if(validateID(ID))
        {
                axios.post('http://87.247.185.122:31003/student',
            {
                    "id": ID,
                    "email": this.state.email,
                    "password": this.state.password,
                    "faculty": "پردیس دانشکده‌های فنی",
                    "field": "مهندسی کامپیوتر",
                    "birthDate": "1399/00/00 ",
                    "level": "کارشناسی",
                    "fname": this.state.firstname ,
                    "lname": this.state.lastname
                 })
                    .then(response =>
                    {
                        console.log("response");
                        console.log(response);
                        if (response.status === 200)
                        {
                            this.handleSignupSucces();
                        }
                    }).catch(error => {
                        console.log("error");
                        console.log(error.response);

                    if(error.response.data.status  === "CONFLICT")
                    {
                        this.handleSignupSuccesError();
                    }
                });
        }else
        {
            console.log("id Error")
            this.handleinvalidID();
        }


        event.preventDefault();
    }

    render()
    {
        return(
            <div>
                <main>
                    <div className="wrapper-signup">
                        <form onSubmit={this.signupHandler.bind(this)}>
                            <img className="Signup-logo" src={logo} alt="Logo"  /><br/><br/>

                            <h6 className="error" > نام </h6>
                            <input
                                type="text"
                                name="firstname"
                                placeholder="نام"
                                onChange={this.handleChange.bind(this)}
                                value={this.state.firstname}
                                required
                            /><br/><br/>
                            <h6 className="error" >نام خانوادگی</h6>
                            <input
                                   type="text"
                                   name="lastname"
                                   placeholder="نام خانوادگی"
                                   onChange={this.handleChange.bind(this)}
                                   value={this.state.lastname}
                                   required
                            /><br/><br/>
                            <h6  className="error" >شماره دانشجویی</h6>
                            <input
                                type="text"
                                name="id"
                                placeholder="شماره دانشجویی"
                                onChange={this.handleChange.bind(this)}
                                value={this.state.id}
                                required
                            /><br/><br/>
                            <h6 className="error" >ایمیل</h6>
                            <input type="email"
                                   name="email"
                                   placeholder="ایمیل"
                                   value={this.state.email}
                                   onChange={this.handleChange.bind(this)}
                                   required
                            /><br/><br/>

                            <h6 className="error" >رمز عبور</h6>
                            <input type="password"
                                   name="password"
                                   placeholder="رمز عبور"
                                   onChange={this.handleChange.bind(this)}
                                   value={this.state.password}
                                   required
                            /><br/><br/>

                            <div id =  "error1" className="error">  </div>
                            <div id =  "message" className="message">  </div>
                            <br/>
                            <Button variant="dark" size="lg" type="submit">ثبت نام</Button>{' '}
                            <Link  to ="/Login"> <Button variant="dark" size="lg" >  ورود</Button> </Link>{' '}

                            <br/>
                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

export default Signup;


