import React from "react";
import {map} from "react-bootstrap/ElementChildren";

export const toFarsiNumber = function(n)
{
    const farsiDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    return n
        .toString()
        .replace(/\d/g, x => farsiDigits[x]);
}

export const  daystoFarsi = function (englishdays)
{
    let dict = {
        "Saturday" : "شنبه",
        "Sunday"   : "یکشنبه",
        "Monday"   : "دوشنبه",
        "Tuesday"  : "سه‌شنبه",
        "Wednesday": "چهارشنبه",
        "Thursday": "پنج‌شنبه",
    };

    let days = englishdays.split(",");
    let persian = [];

    days.map( (day) => {persian.push(dict[day])})
    let persian_days = persian.join(" - ")

    return persian_days
}

export default {toFarsiNumber,daystoFarsi}