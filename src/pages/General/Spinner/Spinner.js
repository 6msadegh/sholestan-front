import Loader from "react-loader-spinner";
import React,{Component} from "react";

class Spinner extends Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="text-center align-middle">
                <Loader  type="Circles" color="blue" height={900} width={768} timeout={500}/>
                <br/>
            </div>

        );
    }
}
export default  Spinner