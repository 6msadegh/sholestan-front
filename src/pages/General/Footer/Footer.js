import React,{Component} from 'react';
import {Button, ModalFooter} from 'react-bootstrap';
import "../../../Assets/Mycollection/font/flaticon.css";
import "./Footer.css"

class Footer extends Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div>
                <br/><br/>
                <ModalFooter className = {`page-footer p-2 ${this.props.page}`}>
                    <div className="container-fluid row align-items-center">
                        <div className="col-lg-8 col-md-8" >
                            <div className="text-right text"> © دانشگاه تهران - سامانه جامع بلبلستان</div>
                        </div>
                        <div className="col-lg-4 col-md-4 social_icons">
                            <div className="float-left">
                                <div className="icons">
                                    <a href="" className="footer"> <i className="flaticon-facebook"> </i></a>
                                    <a href="" className="footer"> <i className="flaticon-linkedin-logo"> </i></a>
                                    <a href="" className="footer"> <i className="flaticon-instagram"> </i></a>
                                    <a href="" className="footer"> <i
                                        className="flaticon-twitter-logo-on-black-background"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalFooter>

            </div>
        );
    }
}

export default Footer;