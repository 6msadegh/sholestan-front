import React, {Component} from 'react';
import {Button, Modal, Nav} from 'react-bootstrap';
import "../../../Assets/Mycollection/font/flaticon.css";
import logo from '../../../Assets/Images/logo.png';

import './Header.css';
import ReactDOM from "react-dom";
import Plan from "../../Plan/Plan";
import Home from "../../Home/Home";
import Courses from "../../Courses/Courses";
import Profile from "../../Profile/Profile";
import axios from "axios";

class Header extends Component
{
    constructor(props) {
        super(props);
    }
    gotoFirstLink(event)
    {
        if (this.props.FirstLink === "انتخاب واحد")
        {
            window.location = "/courses";
        }else if(this.props.FirstLink === "خانه")
        {
            window.location = "/profile";
        }
        event.preventDefault()
    }
    gotoSecondLink(event)
    {
        if (this.props.SecondLink === "انتخاب واحد")
        {
            window.location = "/courses";
        }else if(this.props.SecondLink === "برنامه هفتگی")
        {
            window.location = "/plan";
        }
        event.preventDefault()
    }

    handleexit(event)
    {
        localStorage.clear();
        window.location = "/";

    }
    render() {
        return(
            <div>
                <Nav className="navbar navbar-expand-sm fixed-top nav-style ">
                    <Nav.Link className="navbar-brand">
                        <img className="logo" src={logo} alt="Logo" />
                    </Nav.Link>

                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Nav.Link className="nav-link links" onClick = {this.gotoFirstLink.bind(this)} >{this.props.FirstLink}</Nav.Link>
                        </li>
                        <li className="nav-item">
                            <Nav.Link className="nav-link links" onClick = {this.gotoSecondLink.bind(this)} >{this.props.SecondLink}</Nav.Link>
                        </li>
                    </ul>
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Nav.Link className="nav-link exit" onClick = {this.handleexit.bind(this)}> خروج <i className="flaticon-log-out exit"> </i>  </Nav.Link>
                        </li>
                    </ul>
                </Nav>
            </div>
        );
    }


}

export default Header;