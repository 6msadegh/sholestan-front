import React, {Component} from 'react';
import { Button } from 'react-bootstrap';
import axios from "axios";
import ReactDOM from 'react-dom';

import './Login.css';
import logo from '../../Assets/Images/logo.png';
import {Link} from "react-router-dom";
import {validateEmail} from "../General/Validator";



class Login extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
            {
                email : "",
                password : "",
                loginerror : "",
                data : []
            };
    }

    componentDidMount()
    {
        document.title = "ورود";
    }

    handleChange(event)
    {
        // this.setState({id: event.target.value});
        this.setState({
            [event.target.name] : event.target.value
        })
        console.log("salam")
    }

    handleinvalidEmail()
    {
        console.log("salam");
        const element1 = <h6> ایمیل وارد شده معتبر نمی‌باشد</h6>;
        const element2 = <h6>لطفا دوباره تلاش کنید</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));

    }
    handlePassError()
    {
        console.log("salam");
        const element1 = <h6>رمز عبور با نام کاربری مطابقت ندارد</h6>;
        const element2 = <h6>لطفا دوباره تلاش کنید</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));

    }

    handleSubmit(event)
    {
        const Email = this.state.email;
        const Pass  = this.state.password;


            axios.post('http://87.247.185.122:31003/student/login',
                {
                        "email": Email,
                        "password": Pass
                      }
            ).then(response => {
                console.log(response);
                if (response.status === 200) {
                    // const Data = response.data

                    localStorage.setItem('data', JSON.stringify(response.data));
                    localStorage.setItem('jwt', response.data.jwt);


                    console.log(localStorage.getItem('jwt'));
                    console.log(JSON.parse(localStorage.getItem('data')));
                    this.handleLoginSucces(response.data);
                }
            }).catch(error => {
                console.log(error.response);
                if (error.response.status === 404) {
                    this.handleLoginError();
                }
                if (error.response.status === 403) {
                    this.handlePassError();
                }
            });


        event.preventDefault();
    }

    handleLoginError()
    {
        const element1 = <h6>دانشجویی با این مشخصات یافت نشد :)</h6>;
        const element2 = <h6>لطفا دوباره تلاش کنید</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));
    }

    handleLoginSucces(data)
    {
        window.location = "/profile";
    }

    render()
    {
        return(
            <div>
                <main>
                    <div className="wrapper">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <img className="Login-logo" src={logo} alt="Logo" /><br/><br/>
                            <h6>ایمیل</h6>
                            <input
                                type="email"
                                name="email"
                                placeholder="ایمیل"
                                value={this.state.email}
                                onChange={this.handleChange.bind(this)}
                                required
                            /><br/>
                            <br/>
                            <h6>رمز عبور</h6>
                            <input
                                type="password"
                                name="password"
                                placeholder="رمز عبور"
                                value={this.state.password}
                                onChange={this.handleChange.bind(this)}
                                required
                            /><br/>
                            <br/>
                            <div id =  "error1" className="error">  </div>
                            <div id =  "error2" className="error">  </div>
                            <Link to="/ForgetPass" className= "forgot"> رمز عبور را فراموش کرده‌‌ام ::::( </Link>
                            <br/><br/>
                            <Button variant="dark" size="lg" type="submit">تایید</Button>{' '}
                            <Link to="/signup"> <Button variant="dark" size="lg">ثبت نام</Button> </Link>{' '}
                            <br/><br/>
                            <div id =  "sendemail" className="sendemail">  </div>
                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

export default Login;