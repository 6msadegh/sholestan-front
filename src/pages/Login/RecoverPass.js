import React, {Component} from 'react';
import { Button } from 'react-bootstrap';
import axios from "axios";
import ReactDOM from 'react-dom';

import './Login.css';
import logo from '../../Assets/Images/logo.png';
import {Link} from "react-router-dom";


class RecoverPass extends Component {
    constructor(props) {
        super(props);
        this.state =
            {
                password: "",
                jwt: "",
            };
    }

    componentDidMount() {
        document.title = "بازیابی رمز عبور";

        let search = new URLSearchParams(this.props.location.search);
        console.log(search);
        let Jwt = search.get("jwt");
        this.setState({jwt: Jwt})
        console.log("jwt")
        console.log(Jwt)
    }

    handleChange(event) {
        // this.setState({id: event.target.value});
        this.setState({
            [event.target.name]: event.target.value
        })
        console.log("salam")
    }

    handleSubmit(event) {
        const Pass = this.state.password;
        const JWT = this.state.jwt;

        axios.put('http://87.247.185.122:31003/student/password',
            {
                "password": Pass
            }, {headers: {Authorization:JWT }}
        ).then(response => {
            console.log(response);
            if (response.status === 200) {
                console.log(response.data);

                this.handleRecoverSuccess();

            }
        }).catch(error => {
                    console.log(error);
                 this.handleRecoverfailure();
        });
        event.preventDefault();
    }

    handleRecoverSuccess() {
        const element = <h4> رمز شما با موفقیت تغییر یافت. </h4>;
        const error = <h4> </h4>;
        ReactDOM.render(element, document.getElementById('changedpass'));
        ReactDOM.render(error, document.getElementById('error'));

    }

    handleRecoverfailure()
    {
        const element = <h4> </h4>;
        const error = <h4> لیتک منقضی شده به صفحه ورود رفته و دوباره درخواست لینک کنید. </h4>;
        ReactDOM.render(element, document.getElementById('changedpass'));
        ReactDOM.render(error, document.getElementById('error'));
    }

    render()
    {
        return(
            <div>
                <main>
                    <div className="wrapper">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <img className="Login-logo" src={logo} alt="Logo" /><br/><br/>
                            <input
                                type="password"
                                name="password"
                                placeholder="رمز عبور"
                                value={this.state.password}
                                onChange={this.handleChange.bind(this)}
                                required
                            /><br/>
                            <br/>
                            <Button variant="dark" size="lg" type="submit">تایید</Button>{' '}
                            <Link  to ="/Login"> <Button variant="dark" size="lg" >  ورود </Button> </Link>{' '}
                            <br/><br/>
                            <div id =  "changedpass" className="succes">  </div>
                            <div id =  "error" className="error">  </div>
                            <br/><br/>
                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

export default RecoverPass;