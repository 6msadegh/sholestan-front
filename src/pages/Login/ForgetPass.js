import React, {Component} from 'react';
import { Button } from 'react-bootstrap';
import axios from "axios";
import ReactDOM from 'react-dom';

import './Login.css';
import logo from '../../Assets/Images/logo.png';
import {validateEmail} from "../General/Validator";

class ForgetPass extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
            {
                email : "",
                password : "",
                loginerror : "",
                data : []
            };
    }

    componentDidMount()
    {
        document.title = "فراموشی رمز عبور";
    }

    handleChange(event)
    {
        this.setState({
            [event.target.name] : event.target.value
        })
        console.log("salam")
    }

    handleEmailError()
    {
        console.log("salam");
        const element1 = <h6>دانشجویی با این مشخصات یافت نشد :)</h6>;
        const element2 = <h6>لطفا دوباره تلاش کنید</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));
    }
    handleinvalidEmail()
    {
        console.log("salam");
        const element1 = <h6> ایمیل وارد شده معتبر نمی‌باشد</h6>;
        const element2 = <h6>لطفا دوباره تلاش کنید</h6>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));

    }

    handlesendEmail()
    {
        const element1 = <h6>  </h6>;
        const element2 = <h6> </h6>;
        const element3 = <h4> لینک به ایمیل شما ارسال شد. </h4>;
        ReactDOM.render(element1, document.getElementById('error1'));
        ReactDOM.render(element2, document.getElementById('error2'));
        ReactDOM.render(element3, document.getElementById('sendemail'));
    }

    handleSubmit(event)
    {
        const Email = this.state.email;
        console.log(Email);
        if  (validateEmail(Email))
        {
            axios.post('http://87.247.185.122:31003/student/password' ,
            {
                "email" :  Email
            })
                .then(response => {
                console.log(response);
                if (response.status === 200)
                {
                     this.handlesendEmail();
                }
            }).catch(error => {
                console.log(error.response)
                if(error.response.status  === 401)
                {
                    this.handleEmailError();
                }
            });
        }else
        {
            console.log("Email nok")
            this.handleinvalidEmail();
        }

        event.preventDefault();
    }



    render()
    {
        return(
            <div>
                <main>
                    <div className="wrapper">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <img className="Login-logo" src={logo} alt="Logo" /><br/>
                            <br/><br/><br/>
                            <input
                                type="string"
                                name="email"
                                placeholder="ایمیل"
                                value={this.state.email}
                                onChange={this.handleChange.bind(this)}
                                required
                            /><br/><br/><br/>
                            <h4 className="succes"> ایمیل خود را وارد کنید </h4>
                            <br/><br/>
                            <div id =  "sendemail" className="sendemail">  </div>
                            <div id =  "error1" className="error">  </div>
                            <div id =  "error2" className="error">  </div>
                            <br/><br/>
                            <Button variant="dark" size="lg" type="submit">تایید</Button>{' '}
                            <br/><br/>
                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

export default ForgetPass;