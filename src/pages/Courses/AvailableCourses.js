import React, {Component} from "react";
import {Button, Col, Container, OverlayTrigger, Table, Tooltip} from "react-bootstrap";
import "../../Assets/Mycollection/font/flaticon.css";
import "./Courses.css"
import axios from "axios";
import AvailableCourse from "./AvailableCourse";
import {toFarsiNumber} from "../General/Translator";
import {daystoFarsi} from "../General/Translator";

class AvailableCourses extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                Datas : [],
                Goal : ""
            }
    }

    HandleChange(event)
    {
        this.setState({Goal: event.target.value});
    }

      async componentDidMount() {

          await axios.get('http://87.247.185.122:31003/courses', {headers: {Authorization:   localStorage.getItem('jwt')}})
              .then(response => {
                  this.setState({Datas: response.data});
              })
              .catch(error => {
                  console.log("courses error", error.response);
                  if(error.response.status === 403 || error.response.status === 401)
                  {
                      localStorage.clear();
                      window.location = "/";
                  }
              });

      }

    async HandleSearch(event)
    {
        await axios.get('http://87.247.185.122:31003/courses?courseType=&searchQuery='+this.state.Goal, {headers: {Authorization: localStorage.getItem('jwt')}})
                .then(response => {this.setState({Datas: response.data})})
                    .catch(error => {
                        console.log("courses error", error.response);
                        if(error.response.status === 403 || error.response.status === 401)
                        {
                            localStorage.clear();
                            window.location = "/";
                        }
                    });
    }

    async HandleShowCourses(type)
    {
        await axios.get('http://87.247.185.122:31003/courses?courseType='+ type, {headers: {Authorization: localStorage.getItem('jwt')}})
                .then(response => {this.setState({ Datas: response.data});})
                    .catch(error => {
                        console.log("show course error",error.response);
                        if(error.response.status === 403 || error.response.status === 401)
                        {
                            localStorage.clear();
                            window.location = "/";
                        }
                    });
    }

    render() {
        return(
            <div>
                {console.log("fnbhernfjer bfhejrmf nrebjhfjer fnbhehrjfmre nhfref rnebh")}
                <Container>
                    <Col lg = "9" md = "10" xs="12" className="searchcontainer mx-auto">
                        <div className="input-group">
                            <input  onChange={this.HandleChange.bind(this)} type="search" className = "searchbox rounded mx-auto" placeholder="نام درس" />
                            <Button onClick={this.HandleSearch.bind(this)} type="button" className="btn-outline-primary searchbtn">جستجو
                                <i className="flaticon-loupe float-left"> </i> </Button>
                        </div>
                    </Col>
                </Container>


                <br/><br/><div id="error" > </div><br/><br/>


                <div className="col-md-12">
                    <div className="col-lg-11 col-md-12 report mx-auto">
                        <div className="Box_title">
                            <div className="mx-auto"> دروس ارائه شده</div>
                        </div>
                        <br/>
                        <div className="row dataContainer mx-auto">
                            <div className="col-lg-12 row">
                                <div className="col-md-1"> </div>
                                <div className="col-md-2">
                                    <Button onClick={() => this.HandleShowCourses("")} className=" category">همه</Button>
                                </div>
                                <div className="col-md-2">
                                    <Button onClick={() => this.HandleShowCourses("Takhasosi")} className="category"> اختصاصی</Button>
                                </div>
                                <div className="col-md-2">
                                    <Button onClick={() => this.HandleShowCourses("Asli")} className="category"> اصلی</Button>
                                </div>
                                <div className="col-md-2">
                                    <Button onClick={() => this.HandleShowCourses("Paaye")} className="category"> پایه</Button>
                                </div>
                                <div className="col-md-2">
                                    <Button onClick={() => this.HandleShowCourses("Umumi")}className="category"> عمومی</Button>
                                </div>
                                <div className="col-md-1"> </div>
                            </div>
                        <div>

                        </div>

                            <div className="col-md-12">
                                    <br/>
                                        <hr className="tablehr"/>
                            </div>
                        <Col md="11" lg="11">
                            <Table className="table  lowertable table-hover mytable " >
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>کد</th>
                                    <th>ظرفیت</th>
                                    <th>نوع</th>
                                    <th>نام درس</th>
                                    <th>استاد</th>
                                    <th>واحد</th>
                                </tr>
                                </thead>
                                <tbody id = "CoursesTable" >
                                {console.log(this.state.Datas)}
                                {this.state.Datas.map( (Data) => { return <OverlayTrigger placement ="left" overlay={<Tooltip id ="tooltip-top" >
                                    {toFarsiNumber(Data.classTime.time)}
                                    <br/>
                                    {daystoFarsi(Data.classTime.days.toString())}
                                    <hr className="bg-white"/>

                                    پیش  نیازی ها
                                    {Data.prerequisites.map((Course) => {return <h6> {Course.name} </h6>})}
                                </Tooltip>}>
                                    <tr key={Data.code+Data.classCode}>
                                    <AvailableCourse Data={Data} />
                                    </tr>
                                    </OverlayTrigger>})}
                                </tbody>
                            </Table>
                        </Col>
                        </div>
                    </div>
                </div>
            </div>



        )
    }

}
export default AvailableCourses