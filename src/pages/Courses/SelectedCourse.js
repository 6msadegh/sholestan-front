import React,{Component} from "react";
import {Button} from "react-bootstrap";
import ReactDOM from "react-dom";
import axios from "axios";
import {toFarsiNumber} from "../General/Translator";


class SelectedCourse extends Component
{
    constructor(props) {
        super(props);
        this.state =
            {
                situation_style : "",
                situation:"",
                status:""
            }
    }

    UNSAFE_componentWillMount()
    {
        console.log("UNSAFE_componentWillMount")
        this.setState({status:this.props.coursedata.status})
        this.checksituation()
    }

    checksituation() {
        console.log("checking")
        if(this.props.coursedata.status === "Added")
        {
            this.setState({situation_style:"not-register"})
            this.setState({situation:"ثبت نشده"})
        } else if(this.props.coursedata.status === "In wait queue")
        {
            this.setState({situation_style:"wait"})
            this.setState({situation:"در حال انتظار"})
        } else if(this.props.coursedata.finalized === true)
        {
            console.log("update")
            this.setState({situation_style:"Finalized"})
            this.setState({situation:"ثبت شده"})
        }
    }

    async handledelete(event)
    {
        await axios.delete('http://87.247.185.122:31003/student/schedule/course/'+this.props.coursedata.course.code)
            .then(response => {
                console.log(response.data.enrollments);
                ReactDOM.render(
                    response.data.enrollments.filter( (selected) => (selected.status !=="Deleted") ).map( (selected) => { return <tr key={selected.course.code}>
                        <SelectedCourse coursedata = {selected} />
                    </tr>})
                    ,document.getElementById("selected_courses"))
            ReactDOM.render(<div> </div>,document.getElementById("selected_courses2"))})
            .catch(error => {console.log("delete error: ",error.response);});

        event.preventDefault()
    }

    render() {
        return (
            <>
                {console.log(this.props.coursedata.course.name)}
                {console.log(this.props.coursedata.finalized)}
                {console.log(this.props.coursedata.status)}
                {console.log(this.state.situation_style)}
                {console.log(this.state.situation)}
                <td><Button variant="link" onClick={this.handledelete.bind(this)} > <i className="flaticon-trash-bin cancel "> </i> </Button> </td>
                <td>
                    <div className = {`mx-auto ${this.state.situation_style}`}>{this.state.situation} </div>
                </td>
                <td>{toFarsiNumber(this.props.coursedata.course.code)}-{toFarsiNumber(this.props.coursedata.course.classCode)}</td>
                <td>{this.props.coursedata.course.name}</td>
                <td>{this.props.coursedata.course.instructor}</td>
                <td className="blue">{toFarsiNumber(this.props.coursedata.course.units)}</td>
            </>
        );
    }

}

export default SelectedCourse