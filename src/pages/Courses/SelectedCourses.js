import React, {Component} from "react";
import "./Courses.css"
import "../../Assets/Mycollection/font/flaticon.css";
import {Button, Table} from "react-bootstrap";
import SelectedCourse from "./SelectedCourse";
import axios from "axios";
import ReactDOM from "react-dom";
import {toFarsiNumber} from "../General/Translator";
import {store} from "react-notifications-component";

class SelectedCourses extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                Data : [] ,
                finalizedUnits : ""
            }
    }

    async componentDidMount()
    {
        console.log("salam")
        await axios.get('http://87.247.185.122:31003/student/schedule?justFinalized=true', {headers: {Authorization: localStorage.getItem('jwt')}})
            .then(response => {
                console.log(response.data.enrollments);
                console.log(response.data.finalizedUnits);
                this.setState({finalizedUnits:response.data.finalizedUnits})
                ReactDOM.render(<tr> </tr>,document.getElementById("selected_courses"))
                ReactDOM.render(response.data.enrollments.map( (selected) => { return <tr key={selected.course.code}>
                        <SelectedCourse coursedata = {selected}/>
                    </tr>})
                    ,document.getElementById("selected_courses2"));
            }).catch(error => {console.log("Finalized error: ",error.response);});
    }

  componentDidUpdate(prevProps, prevState, snapshot)
  {
      if (this.state.finalizedUnits !== prevState.finalizedUnits) {
             console.log("hello")
      }
  }

    async handle_finalize(event)
    {
        console.log("salam")
        await axios.put('http://87.247.185.122:31003/student/schedule/finalize', {headers: {Authorization: localStorage.getItem('jwt')}})
            .then(response => {
                console.log(response.data.enrollments);
                console.log("salam2");
                this.setState({finalizedUnits:response.data.finalizedUnits})
                ReactDOM.render(<tr> </tr>,document.getElementById("selected_courses"))
                ReactDOM.render(response.data.enrollments.map( (selected) => { return <tr key={selected.course.code}>
                            <SelectedCourse coursedata = {selected}/>
                        </tr>})
                    ,document.getElementById("selected_courses2"));
            }).catch(error => {
                this.handle_finalize_error(error.response.data.errors);

                console.log("Finalized error: ",error.response.data.errors);} );

        event.preventDefault()
    }

    handle_finalize_error(messages)
    {
        let error = "";
        messages.map( (message) => {
            if(message.substring(0,1)==="P")
            {
                error +=  " پیش نیازی درس با کد";
                error += toFarsiNumber(message.split("").reverse().join("").substring(0,7).split("").reverse().join(""));
                error += "رعایت نشده"
            }else if(message.substring(0,1)==="M")
            {
                error +="حداقل واحد‌ها رعایت نشده است";
            }
            else if(message.substring(0,1)==="R")
            {
                error +="درس  قبلا گذرانده شده";
            }
        })

        store.addNotification({
            title: "خطا",
            direction:"rtl",
            message: error,
            type: "danger",
            insert:"top-right",
            container: "top-right",
            animationIn: ["animate__animated", "animate__fadeIn"],
            animationOut: ["animate__animated", "animate__fadeOut"],
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        });

    }

    async handle_reset(event)
    {
        await axios.put('http://87.247.185.122:31003/student/schedule/reset', {headers: {Authorization: localStorage.getItem('jwt')}})
            .then(response => {
                console.log(response.data.enrollments);
                console.log("salam2");
                ReactDOM.render(<tr> </tr>,document.getElementById("selected_courses"))
                ReactDOM.render(response.data.enrollments.map( (selected) => { return <tr key={selected.course.code}>
                        <SelectedCourse coursedata = {selected}/>
                    </tr>})
                    ,document.getElementById("selected_courses2"));
            }).catch(error => {console.log("Finalized error: ",error.response);});

        event.preventDefault()
    }

    render() {
        return(
            <div>
                <div className="col-md-12">
                    <div className="col-lg-11 col-md-12 mx-auto report">
                        <div className="Box_title">
                            <div className="mx-auto"> دروس انتخاب شده</div>
                        </div><br/>
                        <div className="row dataContainer mx-auto modal-dialog-scrollable">

                            <Table className="table-bordered gray">
                                <thead>
                                <tr>
                                    <th> </th>
                                    <th>وضعیت</th>
                                    <th>کد</th>
                                    <th>نام درس</th>
                                    <th>استاد</th>
                                    <th>واحد</th>
                                </tr>
                                </thead>

                                <tbody id="selected_courses" >


                                </tbody>

                                <tbody id="selected_courses2">

                                </tbody>

                            </Table>


                            <div className="col-md-12">
                                <hr className="tablehr" />
                            </div>
                            <div className="col-lg-3 col-md-3">
                                <p className="succes_courses"> تعداد واحد ثبت شده : {toFarsiNumber(this.state.finalizedUnits)} </p>
                            </div>
                            <div className="col-lg-9 col-md-9">
                                <Button className="return" onClick={this.handle_reset.bind(this)}><i className="flaticon-refresh-arrow"> </i></Button>{' '}
                                <Button  className="finalize" onClick={this.handle_finalize.bind(this)}>ثبت نهایی</Button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
export default SelectedCourses