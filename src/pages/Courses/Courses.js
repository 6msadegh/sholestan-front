import React,{Component} from "react";
import Header from "../General/Header/Header";
import AvailableCourses from "./AvailableCourses";
import SelectedCourses from "./SelectedCourses";
import Spinner from "../General/Spinner/Spinner";
import Footer from "../General/Footer/Footer";

import "./Courses.css"
import ReactNotification from "react-notifications-component";
import 'react-notifications-component/dist/theme.css'

class Courses extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                "Datas" : []
            }
    }

    componentDidMount()
    {
        document.title = "انتخاب واحد";
    }


    render()
    {
        return(
            <div>
                <div className="coursesemptybox"> </div>
                <Header FirstLink = "خانه" SecondLink = "برنامه هفتگی" />
                <Spinner />
                {/*<SelectedCourses data={[]} />*/}
                <br/><br/><br/><br/>
                <ReactNotification />
                <AvailableCourses ID = {this.props.ID} />
                <Footer />
            </div>
        )
    }

}
export default Courses