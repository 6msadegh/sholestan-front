import {Button} from "react-bootstrap";
import React, {Component} from "react";
import "../../Assets/Mycollection/font/flaticon.css";
import "./Courses.css"
import ReactDOM from "react-dom";
import SelectedCourse from "./SelectedCourse";
import axios from "axios";
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css'
import {toFarsiNumber} from "../General/Translator";

class AvailableCourse extends Component
{
    constructor(props) {
        super(props);
        this.state =
            {
                coursetype : "",
                code : "",
                classCode : "",
                style : "",
                takers_num_color: "",
                wait : "" ,
                selected : [],
                isShowing : false
            }
    }

    handletype()
    {
        if(this.props.Data.type === "Asli")
        {
            this.setState({coursetype:"اصلی"});

        }else if(this.props.Data.type === "Paaye")
        {
            this.setState({coursetype:"پایه"});
        }
        else if(this.props.Data.type === "Takhasosi")
        {
            this.setState({coursetype:"تخصصی"});
        }
        else if(this.props.Data.type === "Umumi")
        {
            this.setState({coursetype:"عمومی"});
        }
    }

    componentDidMount()
    {
        this.handletype()
        this.setState({code:this.props.Data.code});
        this.setState({classCode:this.props.Data.classCode});

        if(this.props.Data.capacity <= this.props.Data.takers)
        {
            this.setState({style:"clock-circular-outline"});
            this.setState({takers_num_color:"gray"});
            this.setState({wait:"true"})
        }else
        {
            this.setState({style:"add"});
            this.setState({takers_num_color:"blue"});
            this.setState({wait:"false"})
        }

    }

    async handle_add(event)
    {
         await axios.post('http://87.247.185.122:31003/student/schedule/course/'+this.state.code+'?waitQueue='+this.state.wait,)
                        .then(response => {
                            console.log(response.data.enrollments);
                            ReactDOM.render(
                                response.data.enrollments.map((selected) => { return <tr key={selected.course.code}>
                                        <SelectedCourse coursedata = {selected} />
                                </tr>})
                                ,document.getElementById("selected_courses"))
                            ReactDOM.render(<div> </div>,document.getElementById("selected_courses2"))
                        })
                            .catch(error => {
                                this.handle_add_error(error.response.data.errors)
                                console.log("error: ", error.response.data.errors);

                            })

         event.preventDefault()
    }

    handle_add_error(messages)
    {
        let error = "";
        messages.map( (message) => {if(message.substring(0,1)==="C")
                                    {
                                        error +=  "همزمانی  ساعت کلاس با کلاس کد ";
                                        error += toFarsiNumber(message.split("").reverse().join("").substring(0,7).split("").reverse().join(""));
                                    }
                                   else if(message.substring(0,1)==="E")
                                   {
                                       error += "تداخل ساعت امتحان با امتحان کلاس کد ";
                                       error += toFarsiNumber(message.split("").reverse().join("").substring(0,7).split("").reverse().join(""));
                                   }
        })

        store.addNotification({
            title: "خطا",
            direction:"rtl",
            message: error,
            type: "danger",
            insert:"top-right",
            container: "top-right",
            animationIn: ["animate__animated", "animate__fadeIn"],
            animationOut: ["animate__animated", "animate__fadeOut"],
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        });

    }

    render() {
        return(
            <>
                <td><Button className={this.state.style} onClick={this.handle_add.bind(this)}> <i className={"flaticon-"+this.state.style} > </i> </Button> </td>
                <td>{toFarsiNumber(this.props.Data.code)+"-"+toFarsiNumber(this.props.Data.classCode)}</td>
                <td className = {this.state.takers_num_color}>  {toFarsiNumber(this.props.Data.capacity)} / {toFarsiNumber(this.props.Data.takers) } </td>
                <td >
                    <div className = {`mx-auto ${this.props.Data.type}`} >  {this.state.coursetype} </div>
                </td>
                <td>{this.props.Data.name}</td>
                <td>{this.props.Data.instructor}</td>
                <td className="blue">{toFarsiNumber(this.props.Data.units)}</td>
            </>
        );
    }
}
export default AvailableCourse

