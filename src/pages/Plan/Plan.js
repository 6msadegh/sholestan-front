import React,{Component} from "react";

import Header from "../General/Header/Header";
import "./Plan.css"
import "../Profile/Profile.css"
import "../../Assets/Mycollection/font/flaticon.css"
import {Col, Container, Row, Table} from "react-bootstrap";
import Footer from "../General/Footer/Footer";
import Spinner from "../General/Spinner/Spinner";
import axios from "axios";
import ReactDOM from "react-dom";
import ClassCard from "./ClassCard";

class Plan extends Component
{
    constructor(props) {
        super(props);

    }

    componentDidMount()
    {
        document.title = "برنامه هفتگی";
        axios.get('http://87.247.185.122:31003/student/schedule?justFinalized=true', {headers: {Authorization: localStorage.getItem('jwt')}})
            .then(response => { console.log(response.data.enrollmennts)

                ReactDOM.render(response.data.enrollments.map((course) => {
                            return (
                                <div>
                                    {
                                        course.course.classTime.days.map((day)=>{return(
                                        <div>
                                            <ClassCard
                                                name = {course.course.name}
                                                type = {course.course.type}
                                                time = {course.course.classTime.time}
                                                day = {day}
                                            />
                                        </div>
                                        )})
                                    }
                                </div>
                            )
                    }),document.getElementById("schedual"))

            })
                .catch(error => {console.log("error: ",error);});
    }

    render() {
        return (
            <div>
                <div className="planemptybox"> </div>
                <Spinner />
                <Header FirstLink = "خانه" SecondLink="انتخاب واحد" />
                <Container fluid>
                    <Col md = "12">
                        <Col lg = "11" md = "12" className= "report mx-auto">
                            <Col lg="12" md = "12">
                                <Row >
                                    <div lg = "2" md ="2" className="my-align-left"> <i className="flaticon-calendar calendar"> </i></div>
                                    <div lg = "2" md ="2" className="my-align-right" >برنامه هفتگی</div>
                                    <div lg = "2" md ="2" className="mr-auto">ترم جاری</div>
                                </Row>
                            </Col>

                            <hr className="planhr"/>

                        <Table className="table-bordered plan">
                            <thead>
                            <tr>
                                <th id="schedual">

                                </th>
                                <th>شنبه</th>
                                <th>یکشنبه</th>
                                <th>دوشنبه</th>
                                <th>سه‌شنبه</th>
                                <th>چهارشنبه</th>
                                <th>پنجشنبه</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>۸:۰۰-۹:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۹:۰۰-۱۰:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۰:۰۰-۱۱:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۱:۰۰-۱۲:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۲:۰۰-۱۳:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۳:۰۰-۱۴:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۴:۰۰-۱۵:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۵:۰۰-۱۶:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۶:۰۰-۱۷:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>۱۷:۰۰-۱۸:۰۰</td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            </tbody>
                        </Table>

                        </Col>
                    </Col>
                </Container>

                <Footer page="fixed-bottom" />
            </div>
        );
    }
}
export default Plan