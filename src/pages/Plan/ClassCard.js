import React,{Component} from "react";

import "./Plan.css"
import {toFarsiNumber} from "../General/Translator";

class ClassCard extends Component
{
    constructor(props) {
        super(props);

        this.state =
            {
                starttime : "",
                duration : "",
                type : ""
            }

    }

    componentDidMount() {
        this.findstarttime()
        this.findclassduration()
        this.set_typename()
    }

    set_typename()
    {
        let dict = {
            "Paaye":"پایه",
            "Takhasosi":"تخصصی",
            "Umumi":"عمومی",
            "Asli":"اصلی",
        };
        this.setState({type:dict[this.props.type]})
    }

    findstarttime()
    {
        let dict = {
            "8:00-": "eight",
            "8:30-": "eighth",
            "9:00-": "nine",
            "9:30-": "nineh",
            "10:00": "ten",
            "10:30": "tenh",
            "11:00": "eleven",
            "11:30": "elevenh",
            "12:00": "twelve",
            "12:30": "twelveh",
            "13:00": "thirteen",
            "13:30": "thirteenh",
            "14:00": "fourteen",
            "14:30": "fourteenh",
            "15:00": "fifteen",
            "15:30": "fifteenh",
            "16:00": "sixteen",
            "16:30": "sixteenh"
        };
        this.setState({starttime:dict[this.props.time.substring(0,5)]})
    }

    findclassduration()
    {
        console.log("inja",this.props.time.substring(2,3) )
        console.log("inja",this.props.time.substring(8,9) )
        // console.log(  "hour"+(Number(this.props.time.substring(6,8))- Number(this.props.time.substring(0,2))).toString())
        if(this.props.time.substring(1,2) === this.props.time.substring(6,7)  && this.props.time.substring(1,2) === ":" )
        {
            this.setState({duration:"hour" + (Number(this.props.time.substring(5,6))-Number(this.props.time.substring(0,1))).toString()})
        }else if(this.props.time.substring(1,2) === this.props.time.substring(7,8)  && this.props.time.substring(1,2) === ":")
        {
            this.setState({duration:"hour" + (Number(this.props.time.substring(5,7))-Number(this.props.time.substring(0,1))).toString()})
        }else if(this.props.time.substring(2,3) === this.props.time.substring(8,9)  && this.props.time.substring(1,2) === ":")
        {
            this.setState({duration:"hour" + (Number(this.props.time.substring(6,8))-Number(this.props.time.substring(0,2))).toString()})
        }
    }

    render() {
        return (
            <>
                {/*{console.log(this.props.type)}*/}
                {console.log(this.props.day)}
                {console.log(this.state.starttime)}
                {console.log(this.state.duration)}
                {/*{console.log(this.props.name)}*/}
                <div className={` hourone ${"P"+this.props.type} ${this.props.day} ${this.state.starttime}`}>
                    <div>
                        <div>{toFarsiNumber(this.props.time)}</div>
                        <div>{this.props.name} </div>
                        <div>{this.state.type}</div>
                    </div>
                </div>
            </>
        );
    }
}

export default ClassCard

