import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import logo from '../../Assets/Images/logo.png';

import './Home.css';
import Signup from "../Signup/Signup";
import Login from "../Login/Login";
import ReactDOM from "react-dom";
import {Link} from "react-router-dom";



class Home extends Component
{


    constructor(props)
    {
        super(props);
    }

    componentDidMount()
    {
        document.title = "خانه";
    }

    render()
    {
        return(
            <div>
               <div className="background">
                   <img className="Homelogo" src={logo} alt="Logo" />
                   <br/>
                    <h1> (: به بلبلستان خوش آمدید :)</h1>
                   <br/>
                    <div>
                        <Link to="/login"> <Button variant="outline-info" size="lg">ورود</Button> </Link>{' '}
                        <Link to="/Signup"><Button variant="outline-info" size="lg">ثبت نام</Button> </Link>{' '}
                    </div>
               </div>
            </div>
        );
    }
}

export default Home;